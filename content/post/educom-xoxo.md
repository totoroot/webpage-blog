---
title: "When Service Providers Split Up"
date: 2022-03-14T12:52:27+01:00
draft: true
image: /service-providers/educom-xoxo.png
tags:
    - mobile phone
    - user stories
    - Austria
    - service providers
    - mobile network operators
    - ISPs
    - XOXO
    - Drei
    - educom
    - A1
---

> This post is probably only going to be interesting for you, if you reside in **Austria** and were a former customer of the mobile network operator 'educom'. However you are of course as well more than welcome to continue reading if that's not the case.

# When service providers split up

When mobile network operators fight and split up, as a customer and user, one can feel like a kid whose parents are going through a divorce.

Oblivious like a child often is, when it comes to the health of their parents' marriage, I did not really expect having to think about the relationship with my mobile network operator any time soon, as I was content with the stability and performance of the service they provided, their customer service whenever I had to get in touch, and the amount I paid monthly for both my mobile phone contract as well as the mobile LTE internet that my roommates and I share in our flat..

Especially in the case of service providers, one is often unsure with which of the two one should stay and if deciding for one will have any ramifications, that one cannot possibly assess at the moment they split up.

*I thought about using a [cliché stock photo of a divorced family](https://pixabay.com/de/illustrations/familie-scheidung-trennung-ehe-3090056/), but I dropped the idea.*


## A little backstory on how I ended up happily with 'educom' ...

I had been a customer of the mobile network operator 'educom' for a few years now. First only for my mobile phone contract, as they had compelling offers for students with loads of mobile data for pretty cheap and you were promised to be able to keep your contract also after you were done with university.


### When service providers form unholy alliances

I had originally made the switch, after one of the biggest Austrian mobile network operators 'T-Mobile Austria' merged with one of the biggest ISPs in Austria, namely 'UPC Austria', which incidientally had not long before gotten into the mobile network operator space with their brand 'UPC Mobile' as well.

With the merger, UPC Austria would no longer operate and my contract with them (they had the best offer after I returned to Austria from my year abroad) would no longer be valid and I would have now had to agree to a new contract with the newly founded 'Magenta', which formed with this merger and is now still one of the biggest players in the Austrian mobile network operator and ISP business.

*If you want to read up on this pretty historic company merger [here](https://www.techniknews.net/en/news/t-mobile-and-upc-will-be-magenta-t-off-for-telering-next-year/) is a link to an article about it.*
My very first mobile phone contract was with 'T-Mobile' and since I have had many issues over the years and always felt like they were just out to rip off their customers any chance they got, I wasn't exactly thrilled about now signing a contract with a company which orginated from them, as I expected them to follow suit with their business practices.


### Happy with my new contract

After I had made the switch to 'educom' it was like a breath of fresh air.

Not only did I now have more mobile data than ever before (about 15 GB every month, which you could accumulate for three months for 45GB in total), but it also cost me just 10 Euro per month.

![Happily using my phone at home. Obviously not me.](/service-providers/phone-pixabay.jpg "Photo of woman using her phone sitting down at a table at home.")

*Image taken from [Pixabay](https://pixabay.com/images/id-5190643/). Thanks to [Vinzent Weinbeer](https://pixabay.com/de/users/vinzentweinbeer-5342962/) and the unknown woman in the picture.*

Certainly the best part about them, was their customer service and whenever I had to get in touch, the person on the other end was not only polite and friendly, but they truly made an effort to troubleshoot your issues and resolve them with or for you.

Waiting times on their hotline were also minimal compared to what I had experienced with 'T-Mobile' over the years (my family has told me that it got even worse after the merger and my father has once spent more than an hour listening to most awful elevator music waiting for someone to answer the phone with 'Magenta').

Soon after I've shared my good experiences with my roomates, we also switched to 'educom' for our mobile LTE internet at home which gave us 80/20 Mbit/s rates for about 27 Euro a month and came with a [Huawei B535-232](https://www.4gltemall.com/blog/huawei-4g-router-3-pro-b535-test/), which is a reasonably powerful LTE router.

I never liked products from 'Huawei' and [their ties to the CCP](https://www.cfr.org/backgrounder/huawei-chinas-controversial-tech-giant) are concerning, we did not really find a better solution, as not many good LTE routers exist, and getting wired internet in our flat would have cost too much.


## Why couldn't it have just stayed like that?!

My contract with 'educom' has been transferred to 'XOXO' a couple weeks ago and after my initial facepalm regarding the naming of both their brand and their contracts ('LOVELY', 'SMOOCHY', 'LUSH', 'SNUGGY'), I noticed that pretty much nothing had changed besides the now definitely shittier branding to relate to their target group, university students.

![Relatable Branding for Youngsters like ME 🔥💩](/service-providers/fellow-kids.jpg "Fellow Kids Meme with Steve Buscemi in '30 Rock'")

I still connect to one of the biggest and densest mobile operator networks in Austria, namely 'A1', I'm still supposed to get the same speeds and I still pay the same for both my mobile phone contract and our flat's flat rate (*hehe*).

So the company I formerly had a contract with, 'educom', has apparently made a deal with their former parent company 'A1', so that their customers would reside with the newly founded 'XOXO' by default but were offered the opportunity to agree to a new contract with 'educom', which would now operate in the mobile operator network of 'Drei', often stylised as 3.

According to [netztest.at](https://www.netztest.at/en/Karte) the network of 'Hutchinson Drei' is not quite as dense as that of 'A1' although certainly not gravely.

If you want to do the "netztest" yourself, you can do so at [https://www.netztest.at/](https://www.netztest.at/en/Test) or explore the [Open Data](https://www.netztest.at/en/Opentests).


## A couple months later...

Now that leaves are starting to turn orange here in Austria and I have to put on a sweater when I drink my coffee on the balcony in the morning, I wanted to revisit this post and give an update on what has changed and hasn't since I wrote this post in March of this year.

I decided to give 'XOXO', albeit their terrible branding, a fair chance, and can conclude that I have been mostly happy with their services since. So far there has been no change in pricing and the quality of my connection has stayed as expected.

When I moved to a new appartment in June, I was pleasantly surprised that the 4G connection of my LTE router, that we now depend on in our shared appartment, is actually a little more stable than in my old appartment. Also I did not have any issues with my mobile connection in the area I live in now, so checking the coverage for 'A1' with [netztest.at](https://www.netztest.at/en/Karte) was actually worth it.


## Looking ahead

4G LTE mobile internet is the best option for us at the moment in our shared appartment housing 4 people as we get unlimited data with fairly stable 80/20 MBit/s rates for about 27 Euro.

I recently made a call to 'A1' to ask whether we could expect [fibre internet](https://en.wikipedia.org/wiki/Fiber_to_the_x) at our address soon, and their sales representative told me that currently they cannot provide more than 150/20 MBit/s to us over regular coax (copper) cables, which, at a price point of 40 Euro per month, just does not seem like a good deal at all.

They also told me that it usually takes about six months for fibre to get routed to the house, once the block of houses itself has been connected to the fibre-optic network.

According to [breitbandatlas.at](https://breitbandatlas.gv.at/), which is a public map for looking up the maximum speeds of broadband internet in all of Austria, our area should get download speeds of 1GBit/s and upload speeds of 102MBit/s with A1 already. The other provider in our area is Magenta, which I do not wish to ever have a contract with again, due to bad exeperiences in the part.
However, since the data on breibandatlas.at is from Q4 of 2021 I can only hope, that I can expect much faster fibre internet soon.

![Download speed legend from breitbandatlas.at](/service-providers/breitbandatlas.png "Screenshot of the legend for download speeds from breitbandatlas.at")

With a price of 55 Euros for 500/70 MBit/s the value proposition is certainly a little better with the fastest 'A1' tariff plan and I might consider getting the upgrade to no longer have to rely on mobile internet at home. If you want to check the availability of fibre internet at your address with 'A1' you can check out [their website](https://www.a1.net/verfuegbarkeit).

![Screenshot of A1's availability check, showing that only download speeds of max. 150MBit/s are available for my address.](/service-providers/a1-verfuegbarkeit.png "Screenshot of A1's availability check, showing that only download speeds of max. 150MBit/s are available for my address.")

Of course this is not sponsored in any way whatsoever.

---

*Logos of [educom](https://www.educom.at/) and [XOXO](https://www.xoxo-mobile.at/) taken from their respective websites.*
