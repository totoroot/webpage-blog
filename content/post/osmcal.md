---
author: "totoroot"
title: "OpenStreetMap Calendar"
date: "2021-12-19"
description: How to subscribe to OSM events
categories:
tags: [
    "Blog",
    "Mapping",
    "OSM",
    "OpenStreetMap",
    "Event",
    "Calendar",
    "CalDAV",
    "Android",
    "Thunderbird",
    "Link List"
]
image: "/osmcal/screenshot-austria.png"
draft: false
---

This year I've discovered a new hobby that I really enjoy, namely mapping.

Even though I've used the data contributed by the OpenStreetMap (OSM) community for many years, I have never contributed myself until I signed up July of this year.

I'll talk a bit more about how I got started, what I do exactly and why I enjoy mapping so much in a future post but today I discovered a neat feature provided by OSM that I wanted to share.

On [osmcal.org](https://osmcal.org/) you can view future events of the OSM community and subscribe to the event calendar with CalDAV, the calendar extension of the WebDAV standard which allows to sync these events to any calendar application that supports it.

![Screenshot Event List](osmcal/screenshot-list.png)

For calendar synchronisation on my Android device I use [DAVx5](https://f-droid.org/en/packages/at.bitfire.davdroid/) installed from [F-Droid](https://f-droid.org/en/) and on my Linux machines I sync to the calendar extension of [Thunderbird](https://www.thunderbird.net/en-US/).

A good resource for getting started with CalDAV synchronisation with all your devices is the [Synology Knowledge Center](https://kb.synology.com/da-dk/DSM/tutorial/How_to_Sync_Synology_Calendar_with_CalDAV_Clients)

Currently I'm not particularly interested in other countries' meetups, but plan on taking part in the next meetup of Austria's OSM community which is scheduled for 2022-03-09. More information if you also want to join, you can find more information on the [OSM Wiki page](https://wiki.openstreetmap.org/wiki/Austria/Stammtisch/2022-03-09).

The OpenStreetMap Calendar gives the opportunity to only subscribe to local events by choosing your country from a dropdown menu.

![Screenshot Subscribe Austria](osmcal/screenshot-subscribe.png)

If you wanted to get into mapping in the past but did not know how, then a local meetup might be a good place to get to know some people involved and start mapping.

Cheers,\
Matthias
