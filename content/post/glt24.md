---
author: "totoroot"
title: "Grazer Linuxtage 2024"
date: "2024-04-02"
description: '"Declarative Homelab on NixOS ❄️" and "Securely access your homeserver from anywhere"'
categories: Talks
tags: [
    "Linux",
    "Linuxtage",
    "Nix",
    "NixOS",
    "Networking"
]
slug: "glt24"
image: "/glt24/title-image.png"
draft: false
---

# Grazer Linuxtage 2024



I held two talks at this year's [Grazer Linuxtage](https://www.linuxtage.at/en/).

> **Note**: The talks were given at [HS i1](https://hoersaalinfo.tugraz.at/lecture_halls.php?id=2206) and [HS i2](https://hoersaalinfo.tugraz.at/lecture_halls.php?id=4207) at [TU Graz](https://www.tugraz.at/) [Campus Inffeldgasse](https://www.tugraz.at/tu-graz/universitaet/gebaeude-der-tu-graz/#c69303).

## Declarative Homelab on NixOS ❄️

![Screenshot of the first slide of this talk.](/glt24/title-image.png "Homelab on NixOS - Declarative and Reproducible")

If you are interested in this talk, please take a look at [its page on the Grazer Linuxtage's pretalx site](https://pretalx.linuxtage.at/glt24/talk/T37KQQ/).

There you can find an abstract, a short description of what I was talking about, as well as view the recording of the talk and download the slides if you wish to do so.

Since the slides were generated with the presentation framework [reveal.js](https://revealjs.com/) I wanted to share the [HTML version of these slides](https://blog.thym.at/glt24/declarative-homelab-on-nixos).
You can access the exported PDF version of the slides [here](https://blog.thym.at/glt24/declarative-homelab-on-nixos.pdf).

It is possible to go through the slides with the arrow keys or use the spacebar to look through them in order. If you're looking for a particular slide press the "M" key to get a content menu.

A recording of this talk on [media.ccc.de](https://media.ccc.de/v/glt24-478-fully-declarative-homelab-on-nixos-). A livestream was available during the event as well.

## Securely access your home server from anywhere

![Screenshot of the first slide of this talk.](/glt24/slide-1.png "Secure Access")

If you are interested in this talk, please take a look at [its page on the Grazer Linuxtage's pretalx site](https://pretalx.linuxtage.at/glt24/talk/ULPNCG/).

There you can find an abstract, a short description of what I was talking about, as well as view the recording of the talk and download the slides if you wish to do so.

Since the slides were generated with the presentation framework [reveal.js](https://revealjs.com/) I wanted to share the [HTML version of these slides](https://blog.thym.at/glt24/homelab-networking).
You can access the exported PDF version of the slides [here](https://blog.thym.at/glt24/homelab-networking.pdf).

It is possible to go through the slides with the arrow keys or use the spacebar to look through them in order. If you're looking for a particular slide press the "M" key to get a content menu.

A recording of this talk is available on [media.ccc.de](https://media.ccc.de/v/glt24-479-securely-access-your-home-server-from-anywhere). A livestream was available during the event as well.


I will perhaps fix some mistakes if I notice any, so changes may be made.

If you have any questions regarding my talk or want to give personal feedback feel free to reach out to me via Email or [DeltaChat](https://delta.chat/en/) at chat\[at\]thym.at or through the options listed [here](https://blog.thym.at/about/).

Cheers,\
Matthias
